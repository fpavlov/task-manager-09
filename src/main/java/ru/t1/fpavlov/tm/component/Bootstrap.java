package ru.t1.fpavlov.tm.component;

import ru.t1.fpavlov.tm.api.*;
import ru.t1.fpavlov.tm.controller.CommandController;
import ru.t1.fpavlov.tm.repository.CommandRepository;
import ru.t1.fpavlov.tm.service.CommandService;

import java.util.Scanner;

import static ru.t1.fpavlov.tm.constant.ArgumentConst.*;
import static ru.t1.fpavlov.tm.constant.TerminalConst.*;

/*
 * Created by fpavlov on 06.10.2021.
 */
public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private void listenerCommand(final String command) {
        switch (command) {
            case CMD_HELP:
                commandController.displayHelp();
                break;
            case CMD_VERSION:
                commandController.displayVersion();
                break;
            case CMD_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_INFO:
                commandController.displaySystemInfo();
                break;
            case CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            case CMD_EXIT:
                quite();
                break;
            default:
                commandController.incorrectCommand();
        }
    }

    private void listenerArgument(final String argument) {
        switch (argument) {
            case CMD_SHORT_HELP:
                commandController.displayHelp();
                break;
            case CMD_SHORT_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_SHORT_VERSION:
                commandController.displayVersion();
                break;
            case CMD_SHORT_INFO:
                commandController.displaySystemInfo();
                break;
            case CMD_SHORT_COMMANDS:
                commandController.displayCommands();
                break;
            case CMD_SHORT_ARGUMENTS:
                commandController.displayArguments();
                break;
            default:
                commandController.incorrectArgument();
        }

        System.exit(0);
    }

    public void run(final String[] args) {
        if (areArgumentsAvailable(args)) {
            listenerArgument(args[0]);
            return;
        }

        interactiveCommandProcessing();
    }

    private void interactiveCommandProcessing() {
        String param;
        final Scanner scanner = new Scanner(System.in);

        commandController.displayWelcomeText();

        while (true) {
            System.out.println("-- Please enter a command --");
            param = scanner.nextLine();
            listenerCommand(param);
        }
    }

    private static boolean areArgumentsAvailable(final String[] args) {
        return (args != null && args.length > 0);
    }

    private static void quite() {
        System.exit(0);
    }

}
